const E = {
  CLICK: 'click',
  PROGRESS: 'progress',
  TIME_UPDATE: 'timeupdate'
};

var audio = document.getElementById('audio-player'),
    play = document.getElementById('play'),
    pause = document.getElementById('pause'),
    stop = document.getElementById('stop'),
    progress = document.getElementById('progress');

play.addEventListener(E.CLICK, event => {
  audio.play();
});

pause.addEventListener(E.CLICK, event => {
  audio.pause();
});

audio.addEventListener(E.PROGRESS, () => {
    var duration =  audio.duration;
    if (duration > 0) {
        for (var i = 0; i < audio.buffered.length; i++) {
            if (audio.buffered.start(audio.buffered.length - 1 - i) < audio.currentTime) {
                console.log((myAudio.buffered.end(myAudio.buffered.length - 1 - i) / duration) * 100 + "%");
            }
        }
    }
});

audio.addEventListener(E.TIME_UPDATE, () => {
    var duration = audio.duration;
    if (duration > 0) {
        var progress_percent = (audio.currentTime / duration)*100
        console.log(progress_percent + "%");
        progress.value = progress_percent + "";
    }
});

// durationchange
// ended
// pause
// play
// timeupdate
// volumechange
// canplay
// canplaythrough
// progress

